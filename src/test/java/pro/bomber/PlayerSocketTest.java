/* Copyright 2010 Sabre Holdings */
package pro.bomber;


import org.eclipse.jetty.websocket.WebSocket;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PlayerSocketTest
{
    private GameManager gameManager;
    private PlayerSocket playerSocket;

    @Before
    public void init()
    {
        gameManager = mock(GameManager.class);
        playerSocket = new PlayerSocket(gameManager);
    }

    @Test
    public void onOpenShouldAddHimselfToGameManager()
    {
        //when
        playerSocket.onOpen(mock(WebSocket.Connection.class));
        //then
        verify(gameManager).handleAction(any(AddPlayerAction.class));
    }

    @Test
    public void onOpenShouldAssignConnection()
    {
        //given
        WebSocket.Connection connection = mock(WebSocket.Connection.class);
        //when
        playerSocket.onOpen(connection);
        //then
        assertThat(playerSocket.getConnection()).isSameAs(connection);
    }
}
