/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class GameEngineTest
{
    private GameEngine gameEngine = new GameEngine();

    @Test
    public void shouldAddNewPlayerToBoard()
    {
        //given
        Player player = mock(Player.class);
        //when
        gameEngine.addNewPlayer(player);
        //then
        assertThat(gameEngine.contains(player)).isTrue();
    }
    
    @Test
    public void shouldMarshallStateToString(){
    	//given
    	gameEngine.addNewPlayer(new Player(null).withId(1));
    	gameEngine.addNewPlayer(new Player(null).withId(2));
    	//when
    	String marshalled = gameEngine.marshall();
    	//then
    	assertThat(marshalled).isEqualTo("1,100.0,100.0;2,100.0,100.0");
    }
    
    @Test
    public void shouldMovePlayerOnBoard(){
    	//given
    	Player player = new Player(null).withId(1);
		gameEngine.addNewPlayer(player);
    	gameEngine.addNewPlayer(new Player(null).withId(2));
    	
    	//when
    	gameEngine.movePlayer("1", "1","1");
    	//then
    	assertThat(player.getX()).isEqualTo(101);
    }
}
