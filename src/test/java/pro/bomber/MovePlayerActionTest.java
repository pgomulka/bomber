package pro.bomber;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.eclipse.jetty.websocket.WebSocket;
import org.junit.Before;
import org.junit.Test;

public class MovePlayerActionTest {

	private MovePlayerAction movePlayerAction;
	private GameEngine gameEngine;
	@Before
	public void init() {
		gameEngine = mock(GameEngine.class);
		
	}
	@Test
	public void shouldInvokeGameEnginesAddMethod() {
		//given
		MovePlayerAction movePlayerAction =  new MovePlayerAction("1,1,1");
		// when
		movePlayerAction.executeOn(gameEngine);
		// then
		verify(gameEngine).movePlayer("1","1","1");
	}
}
