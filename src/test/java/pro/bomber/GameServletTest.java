/* Copyright 2010 Sabre Holdings */
package pro.bomber;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.websocket.WebSocket;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

public class GameServletTest
{

    private GameServlet gameServlet = new GameServlet();

    @Test
    public void shouldReturnPlayerSocket()
    {
        //when
        WebSocket webSocket = gameServlet.doWebSocketConnect(mock(HttpServletRequest.class), "");
        //then
        assertThat(webSocket).isInstanceOf(PlayerSocket.class);
    }

    @Test
    public void shouldUseSameInstanceOfGameManager()
    {
        //when
        PlayerSocket playerSocket1 = (PlayerSocket) gameServlet.doWebSocketConnect(mock(HttpServletRequest.class), "");
        PlayerSocket playerSocket2 = (PlayerSocket) gameServlet.doWebSocketConnect(mock(HttpServletRequest.class), "");
        //then
        assertThat(playerSocket1.getGameManager()).isNotNull();
        assertThat(playerSocket1.getGameManager()).isSameAs(playerSocket2.getGameManager());
    }
}
