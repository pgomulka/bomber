/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class UpdatePlayersTest
{
    @Mock
    private GameManager gameManager;
    private UpdatePlayers updatePlayers;

    @Before
    public void init()
    {
        updatePlayers = new UpdatePlayers(gameManager);
    }

    @Test
    public void shouldSendUpdateToAllPlayers() throws IOException
    {
        //when
        updatePlayers.run();
        //then
        verify(gameManager).updatePlayers();
    }
}
