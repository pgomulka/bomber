/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import static java.util.Arrays.asList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Matchers.anyString;

import java.io.IOException;


import org.eclipse.jetty.websocket.WebSocket.Connection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;


@RunWith(MockitoJUnitRunner.class)
public class GameManagerTest {
	@Mock
	private GameEngine gameEngine;
	private GameManager gameManager = new GameManager();

	@Before
	public void init() {
		gameManager.setGameEngine(gameEngine);
	}

	@Test
	public void shouldExecuteAction() {
		// given
		AddPlayerAction action = mock(AddPlayerAction.class);
		// when
		gameManager.handleAction(action);
		// then
		verify(action).executeOn(gameEngine);
	}

	@Test
	public void shouldSentGameStateToAllPlayers() throws IOException {
		// given
		Connection connection = mock(Connection.class);
		given(gameEngine.getAllPlayers()).willReturn(asList(new Player(connection),new Player(connection)));
		// when
		gameManager.updatePlayers();
		// then
		verify(connection,times(2)).sendMessage(anyString());
	}
}
