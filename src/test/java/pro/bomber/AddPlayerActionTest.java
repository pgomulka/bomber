/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import java.io.IOException;

import org.eclipse.jetty.websocket.WebSocket;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class AddPlayerActionTest {
	private AddPlayerAction addPlayerAction;
	private GameEngine gameEngine;
	private PlayerFactory playerFactory;
	private WebSocket.Connection connection;
	private Player player;

	@Before
	public void init() {
		playerFactory = mock(PlayerFactory.class);
		connection = mock(WebSocket.Connection.class);
		gameEngine = mock(GameEngine.class);
		addPlayerAction = new AddPlayerAction(connection);
		addPlayerAction.setPlayerFactory(playerFactory);
		player = new Player(connection);
		given(playerFactory.create(any(WebSocket.Connection.class)))
				.willReturn(player);
	}

	@Test
	public void shouldInvokeGameEnginesAddMethod() {
		// when
		addPlayerAction.executeOn(gameEngine);
		// then
		verify(gameEngine).addNewPlayer(any(Player.class)); // why it wont work
															// with local player
	}

	@Test
	public void shouldSendIdToClient() throws IOException {
		// when
		addPlayerAction.executeOn(gameEngine);
		// then
		verify(connection).sendMessage("o "+player.getId());
	}

}
