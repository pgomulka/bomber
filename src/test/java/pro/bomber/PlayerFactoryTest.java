/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import org.eclipse.jetty.websocket.WebSocket;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class PlayerFactoryTest
{
    PlayerFactory playerFactory = new PlayerFactory();

    @Before
    public void init()
    {
        playerFactory.nextId = 0;
    }

    @Test
    public void shouldCreateNewPlayer()
    {
        //given
        WebSocket.Connection connection = mock(WebSocket.Connection.class);
        //when
        Player player = playerFactory.create(connection);
        //then
        assertThat(player).isNotNull();
        assertThat(player.getConnection()).isSameAs(connection);
        assertThat(player.getX()).isEqualTo(100.0);
        assertThat(player.getY()).isEqualTo(100.0);
        assertThat(player.getId()).isEqualTo(0);
    }

    @Test
    public void shouldGenerateNewIds()
    {
        //when
        int id1 = PlayerFactory.getNewId();
        int id2 = PlayerFactory.getNewId();

        //then
        assertThat(id1).isEqualTo(0);
        assertThat(id2).isEqualTo(1);
    }
}
