define([], function ()
{
    var webSocket = undefined;
    var localState = undefined;


    var setSocket = function(socket, state){
        webSocket = socket;
        localState = state;
    };
    var Key = {
      _pressed: {},

      LEFT: 37,
      UP: 38,
      RIGHT: 39,
      DOWN: 40,

      isDown: function(keyCode) {
        return this._pressed[keyCode];
      },

      onKeydown: function(event) {
        this._pressed[event.keyCode] = true;
      },

      onKeyup: function(event) {
        delete this._pressed[event.keyCode];
      }
    };
    var hookKeys = function(){
        window.addEventListener('keyup', function(event) { Key.onKeyup(event); }, false);
        window.addEventListener('keydown', function(event) { Key.onKeydown(event); }, false);
    };
    var init = function(){
        hookKeys();
    };
    var shortcuts = function (e)
    {
        var command = "";
        var playerId = localState.playerId;
        if (e.keyCode == 37)
        {      //left
            command = "m," + playerId + ",-1,0";
            e.preventDefault();
        }
        if (e.keyCode == 38)
        {  //up
            command = "m," + playerId + ",0,-1";
            e.preventDefault();
        }
        if (e.keyCode == 39)
        {//right
            command = "m," + playerId + ",1,0";
            e.preventDefault();
        }
        if (e.keyCode == 40)
        {//down
            command = "m," + playerId + ",0,1";
            e.preventDefault();
        }
        if (e.keyCode == 8)
        {//space
            command = "b," + playerId
            e.preventDefault();
        }
        if (command !== "")
        {
            webSocket.send(command);
        }

    }

    return {
        shortcuts : shortcuts,
        setSocket : setSocket,
        init: init,
        keys: Key
    }


});
