define(["jquery", "easeljs", "net", "keyboard"],
function ($, ejs, net, keyboard)
{


    var
            SERVER = 'localhost',
            PORT = '8080',
            PATH = '/Bomber/anything',
            GAME_URL = 'ws://' + SERVER + ':' + PORT + PATH;

    var
            SERVER_UPDATE_TICK = 50; // update server every x ms

    var
            lastUpdateTime = 0;

    var
            moveSpeed = 1;

    var ejs;


    var
            webSocket = undefined,
            keys = undefined,
            stage = undefined,
            colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple'],
            players = {};
    var
            localState = {
                playerId : undefined,
                xVelocity : 0,
                yVelocity : 0

            },
            lastServerUpdate = {
                xMove : 0,
                yMove : 0,
                xPosition : 100,
                yPosition : 100
            };

    var messageHandler = {
        onOpen:function (data)
        {
            localState.playerId = data;
        },
        onMessage:function (data)
        {
            console.log('MESSAGE: ' + data);
            if (data[0] == "o")
            {
                localState.playerId = data.split(" ")[1];
            }
            else if (data[0] == "w")
            {
                drawBoard(data.split(" ")[1]);
            }
            else if (data[0] == "r")
            {
                var objId = data.split(",")[1];
                removePlayer(objId);
            }
            else if (data[0] == "d")
            {
                death();
            }
            else if (data[0] == "t")
            {
                addText(data.substr(1));
            }
            else
            {
                handleCoords(data);
            }

        },
        onClose:function (data)
        {

        }

    };
    var
            initNetwork = function ()
            {
                webSocket = net.createWebSocket(GAME_URL, messageHandler);
            },

            initKeyboard = function ()
            {
                keyboard.setSocket(webSocket, localState);
                keyboard.init();
                keys = keyboard.keys;

                //$(window).keydown(keyboard.shortcuts);
            },
            initGraphic = function ()
            {
                stage = new ejs.Stage("gameCanvas");

                //init game loop
                ejs.Ticker.setPaused(true);
                ejs.Ticker.useRAF = true;
                ejs.Ticker.setFPS(30);
                ejs.Ticker.addListener(gameLoop);

            },
            updateServer = function(){

                if( (new Date().getTime() - lastUpdateTime) > SERVER_UPDATE_TICK ){
                        lastServerUpdate.xMove = players[localState.playerId].x - lastServerUpdate.xPosition;
                        lastServerUpdate.yMove = players[localState.playerId].y - lastServerUpdate.yPosition;

                        lastServerUpdate.xPosition = players[localState.playerId].x;
                        lastServerUpdate.yPosition = players[localState.playerId].y;

                        if(lastServerUpdate.xMove != 0 && lastServerUpdate.yMove != 0){
                            webSocket.send(['m',localState.playerId,lastServerUpdate.xMove, lastServerUpdate.yMove].join(','));
                            console.log('sent: ' + ['m',localState.playerId,lastServerUpdate.xMove, lastServerUpdate.yMove].join(',') )
                        }
                        lastUpdateTime = new Date().getTime();

                }


            },
            updateStateFromKeys = function(){

                localState.xVelocity = 0;
                localState.yVelocity = 0;
                if(keys.isDown(keys.RIGHT))
                    localState.xVelocity += moveSpeed;
                if(keys.isDown(keys.LEFT))
                    localState.xVelocity -= moveSpeed;
                if(keys.isDown(keys.UP))
                    localState.yVelocity -= moveSpeed;
                if(keys.isDown(keys.DOWN))
                    localState.yVelocity += moveSpeed;

            },

            movePlayers = function(){
                players[localState.playerId].x += localState.xVelocity;
                players[localState.playerId].y += localState.yVelocity;
            },

            gameLoop= {
                tick : function(){
                    stage.update();

                    updateServer();

                    updateStateFromKeys();

                    movePlayers();

                }
            },


            death = function ()
            {
                txt = new ejs.Text("Game Over", "36px Arial", "#000");
                txt.x = 200;
                txt.y = 200;
                stage.addChild(txt);
                //stage.update();
            },

            removePlayer = function (id)
            {
                var objToRemove = players[id];
                stage.removeChild(objToRemove);

            },
            drawBoard = function(coords)
            {
                splited = coords.split(";");
                for(i=0;i<splited.length;i++){
                    handleCoords(splited[i]);
                }
            },
            handleCoords = function (coords)
            {
                var id = coords.split(",")[0];
                var _x = coords.split(",")[1];
                var _y = coords.split(",")[2];
                if (id in players)
                {
                    players[id].setTransform(
                        parseFloat(_x),
                        parseFloat(_y)
                    );
                }
                else
                {
                    createPlayerWithIdAtXY(id,_x,_y);

                    if(ejs.Ticker.getPaused())
                        startGame();
                }
            },
            createPlayerWithIdAtXY = function(id, x, y){

                    var shape = new ejs.Shape();

                    shape.graphics.beginFill(colors[id % colors.length]).
                        drawCircle(0,0,20);
                    shape.x = parseInt(x);
                    shape.y = parseInt(y);

                    players[id] = shape;
                    stage.addChild(shape);

            }

            addText = function (text)
            {
                var $textarea = $('#messages');
                $textarea.val($textarea.val() + text + "\n");
                $textarea.animate({
                    scrollTop:$textarea.height()
                }, 1000);
            },

            sendMessage = function ()
            {
                var message = $('#username').val() + ":" + $('#message').val();
                webSocket.send("t," + message);
                $('#message').val('');
            },

            startGame = function(){
                ejs.Ticker.setPaused(false);
            }

            loadit = function ()
            {
                //bacause EaselJS doesn't support module patter
                ejs = window.createjs;


                initGraphic();
                initNetwork();
                initKeyboard();

                //TODO: check if necessary and remove
                $('body').css('text-align', 'center');
                $('canvas').css('position', '');

            }

    return {
        start:loadit
    }

});

