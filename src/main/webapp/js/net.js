define([], function ()
{

    var createWebSocket = function (address, messageHandler)
    {
        var webSocket = new WebSocket(address);
        webSocket.onopen = function (event)
        {
            messageHandler.onOpen(event.data);
        }
        webSocket.onmessage = function (event)
        {
            messageHandler.onMessage(event.data);

        }
        webSocket.onclose = function (event)
        {
            messageHandler.onClose(event.data);
        }
        return webSocket;
    };

    return {
        createWebSocket : createWebSocket

    }

});