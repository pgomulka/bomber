import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * This class represents a servlet starting a webSocket application
 *
 * @author Andy Moncsek, Trivadis CH
 *
 */
public class GameServlet extends WebSocketServlet
{

    /**
     *
     */
    private static final long serialVersionUID = 5610501345675935366L;
    public final Set<PlayerSocket> users = new CopyOnWriteArraySet<PlayerSocket>();
    public Board board = new Board();

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException
    {
        getServletContext().getNamedDispatcher("default").forward(request,
                response);
    }

    @Override
    public WebSocket doWebSocketConnect(HttpServletRequest arg0, String arg1)
    {
        return new PlayerSocket(board);
    }

}
