/* Copyright 2010 Sabre Holdings */

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class BombTimeout implements Runnable
{
    private final PlayerSocket bomb;
    private Board board;

    public BombTimeout(PlayerSocket bomb, Board board)
    {
        this.bomb = bomb;
        this.board = board;
    }

    @Override
    public void run()
    {
        System.out.println("removing "+bomb);


        board.getBombs().remove(bomb);
        Set<PlayerSocket> toRemove = new HashSet<PlayerSocket>();
        for(PlayerSocket player : board.getPlayers()){
            if(distance(bomb.getInfo(),player.getInfo()) < 50){
                player.sendCommnad("d");
                toRemove.add(player);
                player.getInfo().die();
            }
        }
        for(PlayerSocket player : board.getPlayers()){
            player.sendCommnad("r,"+bomb.getInfo().getId()) ;
            for(PlayerSocket playerToRemove: toRemove){
                player.sendCommnad("r,"+playerToRemove.getInfo().getId());
            }
        }

    }
    public double distance(PlayerInfo playerA, PlayerInfo playerB){
               return Math.sqrt(Math.pow(playerA.getX() - playerB.getX(),2)+ (Math.pow(playerA.getY() - playerB.getY(),2)  ));
    }
}
