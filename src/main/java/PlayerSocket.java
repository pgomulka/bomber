import java.io.IOException;
import java.util.Collection;

import org.eclipse.jetty.websocket.WebSocket.OnTextMessage;

public class PlayerSocket implements OnTextMessage
{
    private Board board;
    private PlayerInfo info;
    private Connection connection;

    public PlayerSocket(Board board)
    {
        this.board = board;
    }

    public void onMessage(String data)
    {

        System.out.println("RECEIVED: " + data);

        String[] splitedData = data.split(",");
        if (splitedData[0].equals("m"))
        {
            String id = splitedData[1];
            int x = Integer.parseInt(splitedData[2]);
            int y = Integer.parseInt(splitedData[3]);

            PlayerSocket moved = board.movePlayer(id, x, y);
            System.out.println(moved);
        }
        else if (splitedData[0].equals("t"))
        {
            String msg = splitedData[1];
            board.sendMsgToChat(msg);
        }else if (splitedData[0].equals("b"))
        {
            String id = splitedData[1];
            board.putBomb(id);
        }
    }

    @Override
    public void onOpen(Connection connection)
    {
        this.info = new PlayerInfo(board.getNewId(), 100, 100);
        this.connection = connection;
        board.addAndUpdateClients(this);
    }

    @Override
    public void onClose(int closeCode, String message)
    {
        board.remove(this);

    }

    public void sendCommnad(String command)
    {
        try
        {
            if (connection != null && connection.isOpen())
            {
                connection.sendMessage(command);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void sendCoords(PlayerSocket player)
    {
        try
        {
            if (connection != null && connection.isOpen())
            {
                connection.sendMessage(player.getInfo().toString());
            }
            System.out.println("TO: " + info.getId() + " MSG:"
                    + player.getInfo());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public void sendInit(Collection<PlayerSocket> otherPlayers)
    {
        try
        {
            if (connection != null && connection.isOpen())
            {
                connection.sendMessage("o " + info.getId());
            }
            for (PlayerSocket player : otherPlayers)
                sendCoords(player);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public String toString()
    {
        return "PLAYER: " + info;
    }

    public void move(int x, int y)
    {
        info.setX(info.getX() + x);
        info.setY(info.getY() + y);
    }

    public PlayerInfo getInfo()
    {
        return info;
    }

    public void setInfo(PlayerInfo info)
    {
        this.info = info;
    }

    public PlayerSocket withInfo(PlayerInfo info)
    {
        this.info = info;
        return this;
    }


}
