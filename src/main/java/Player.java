/* Copyright 2010 Sabre Holdings */

import org.eclipse.jetty.websocket.WebSocket;

import java.io.IOException;

public class Player
{
    public int id;
    public int x;
    public int y;
    public WebSocket.Connection connection;
    public PlayerSocket socket;

    public Player(int id, int x, int y, WebSocket.Connection connection, PlayerSocket socket)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.connection = connection;
        this.socket = socket;
    }

    public void sendCoords(Player player)
    {
        try
        {
            connection.sendMessage(player.id + "," + player.x + "," + player.y);
            System.out.println("TO: "+ id +" MSG:" + player.id + "," + player.x + "," + player.y);
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public String toString(){
        return "PLAYER: " + id + " " +x +" "+ y ;
    }

    public void sendInit(Board board)
    {
        try
        {
            connection.sendMessage("o " + id);
//            for (Player player : board.players)
//                sendCoords(player);


        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


}
