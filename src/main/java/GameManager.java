/* Copyright 2010 Sabre Holdings */

public class GameManager
{
    private Board board;

    public GameManager(Board board)
    {
        this.board = board;
    }

    public boolean isMoveValid(PlayerInfo oldPos, PlayerInfo newPos)
    {
        if (isXAxisValid(newPos) && isYAxisValid(newPos))
        {
            return true;
        }
        return false;
    }

    private boolean isYAxisValid(PlayerInfo newPos)
    {
        return newPos.getY() < board.getHegiht() && newPos.getY() > 0;
    }

    private boolean isXAxisValid(PlayerInfo newPos)
    {
        return newPos.getX() < board.getWidth() && newPos.getX() > 0;
    }
}
