/* Copyright 2010 Sabre Holdings */

import org.eclipse.jetty.util.thread.QueuedThreadPool;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Board
{
    private Map<String, PlayerSocket> players = new LinkedHashMap<String, PlayerSocket>();
    private Set<PlayerSocket> bombs = new LinkedHashSet<PlayerSocket>();
    private int idSeq = 0;
    private int width = 600;
    private int hegiht = 400;



    private GameManager gameManager = new GameManager(this);
    private ScheduledExecutorService bombExecutor = Executors.newScheduledThreadPool(10);

    public void putBomb(String id)
    {
        PlayerInfo bombInfo = new PlayerInfo(players.get(id).getInfo());
        bombInfo.setId(getNewId());

        PlayerSocket bomb = new PlayerSocket(this).withInfo(bombInfo);
        bombs.add(bomb);
        updatePlayers(bomb);

        bombExecutor.schedule(new BombTimeout(bomb, this), 3, TimeUnit.SECONDS);
    }

    public void addAndUpdateClients(PlayerSocket newPlayer)
    {
        players.put("" + newPlayer.getInfo().getId(), newPlayer);

        Collection<PlayerSocket> combined = new HashSet<PlayerSocket>();
        combined.addAll(players.values());
        combined.addAll(bombs);
        newPlayer.sendInit(combined);

        updatePlayers(newPlayer);
        System.out.println("updated");
    }

    private void updatePlayers(PlayerSocket player)
    {
        for (PlayerSocket p : players.values())
        {
            p.sendCoords(player);
        }
    }

    public PlayerSocket movePlayer(String id, int x, int y)
    {
        PlayerSocket player = players.get(id);
        if (!player.getInfo().isDead())
        {
            PlayerInfo newPlayerInfo = new PlayerInfo(player.getInfo()).move(x, y);
            if (gameManager.isMoveValid(player.getInfo(), newPlayerInfo))
            {
                player.setInfo(newPlayerInfo);
            }

            updatePlayers(player);
        }
        return player;
    }
    public void sendMsgToChat(String msg)
    {
        for(PlayerSocket player : players.values()){
            player.sendCommnad("t "+msg);
        }
    }
    public void remove(PlayerSocket socket)
    {
        PlayerSocket removed = players.remove(socket.getInfo().getId());
        System.out.println("REMOVED: " + removed);
    }

    public Collection<PlayerSocket> getPlayers()
    {
        return players.values();
    }

    public int getWidth()
    {
        return width;
    }

    public int getHegiht()
    {
        return hegiht;
    }


    public int getNewId()
    {
        return idSeq++;
    }

    public Set<PlayerSocket> getBombs()
    {
        return bombs;
    }


}
