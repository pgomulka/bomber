/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import org.eclipse.jetty.websocket.WebSocket;

public class PlayerFactory
{
    static int nextId = 0;

    public Player create(WebSocket.Connection connection)
    {
        return new Player(connection).withId(getNewId());
    }

    public static int getNewId()
    {
        return nextId++;
    }
}
