/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import java.io.IOException;

import org.eclipse.jetty.websocket.WebSocket;

public class AddPlayerAction implements Action {
	private PlayerFactory playerFactory = new PlayerFactory();
	private WebSocket.Connection connection;

	public AddPlayerAction(WebSocket.Connection connection) {
		this.connection = connection;
	}

	@Override
	public void executeOn(GameEngine gameEngine) {
		Player player = playerFactory.create(connection);
		gameEngine.addNewPlayer(player);
		try {
			connection.sendMessage("o " + player.getId());
			System.out.println(" wysylam id do gracza : " + player.getId());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setPlayerFactory(PlayerFactory playerFactory) {
		this.playerFactory = playerFactory;
	}
}
