/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import java.io.IOException;

public class UpdatePlayers implements Runnable
{
    private GameManager gameManager;

    public UpdatePlayers(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }

    @Override
    public void run()
    {
        try {
			gameManager.updatePlayers();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
