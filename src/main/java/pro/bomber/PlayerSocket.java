/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import org.eclipse.jetty.websocket.WebSocket;

public class PlayerSocket implements WebSocket.OnTextMessage
{

    private GameManager gameManager;
    private Connection connection;

    public PlayerSocket(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }

    @Override
    public void onMessage(String data)
    {
    	System.out.println(data);
        MovePlayerAction movePlayerAction = new MovePlayerAction(data.substring(2));
        gameManager.handleAction(movePlayerAction);
    }

    @Override
    public void onOpen(Connection connection)
    {
        this.connection = connection;
        AddPlayerAction addPlayerAction = new AddPlayerAction(connection);
        gameManager.handleAction(addPlayerAction);
    }

    @Override
    public void onClose(int i, String s)
    {
    	System.out.println("onClose: " + s +" :"+ i);
    	//gameManager.handleAction(new RemovePlayer());
        
    }

    public GameManager getGameManager()
    {
        return gameManager;
    }

    public Connection getConnection()
    {
        return connection;
    }
}
