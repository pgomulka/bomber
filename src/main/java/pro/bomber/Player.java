/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.eclipse.jetty.websocket.WebSocket.Connection;

public class Player {
	private Connection connection;
	private double x = 100.0;
	private double y = 100.0;
	private int id;

	public Player(Connection connection) {
		this.connection = connection;
	}

	public Connection getConnection() {
		return connection;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public Player withId(int id) {
		this.id = id;
		return this;
	}

	public int getId() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		Player other = (Player) o;
		return new EqualsBuilder().append(id, other.id).equals(o);
	}

	@Override
	public int hashCode() {
		return id;
	}

	public void moveX(double vX) {
		x += vX;
	}

	public void moveY(double vY) {
		y += vY;
	}
}
