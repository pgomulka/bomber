/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import java.util.Collection;
import java.util.Iterator;

public class GameEngine {
	private Board board = new Board();

	public void addNewPlayer(Player player) {
		board.add(player);
	}
	public void removePlayer(Player player){
		board.remove(player);
	}

	public boolean contains(Player player) {
		return board.contains(player);
	}

	public Collection<Player> getAllPlayers() {
		return board.getPlayers();
	}

	public String marshall() {
		StringBuilder sb = new StringBuilder();
		sb.append("w ");
		for (Iterator<Player> iterator = board.getPlayers().iterator()
				;iterator.hasNext();) {
			Player player = iterator.next();
			sb.append("" + player.getId() + "," + player.getX() + ","+ player.getY() );
			if(iterator.hasNext())
				sb.append(";");
		}
		
		return sb.toString();
	}

	public void movePlayer(String id, String x, String y) {
		Player player = board.getPlayer(Integer.parseInt(id));
		player.moveX(Double.parseDouble(x));
		player.moveY(Double.parseDouble(y));
	}
	
	public void removePlayer(String id){
		removePlayer(board.getPlayer(Integer.parseInt(id)));
	}
}
