package pro.bomber;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameServlet extends WebSocketServlet
{
    private ScheduledExecutorService updatePlayersExecutor = Executors.newScheduledThreadPool(1);
    private GameManager gameManager = new GameManager();

    public GameServlet()
    {
        updatePlayersExecutor.scheduleAtFixedRate(new UpdatePlayers(gameManager), 1000, 1000, TimeUnit.MILLISECONDS);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        getServletContext().getNamedDispatcher("default").forward(request, response);
    }

    @Override
    public WebSocket doWebSocketConnect(HttpServletRequest arg0, String arg1)
    {
        return new PlayerSocket(gameManager);
    }

}
