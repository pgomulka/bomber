package pro.bomber;

public interface Action {
	void executeOn(GameEngine gameEngine);
}
