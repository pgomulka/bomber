/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import java.io.IOException;

public class GameManager {
	
	private GameEngine gameEngine = new GameEngine();

	public void handleAction(Action action) {
		action.executeOn(gameEngine);
	}

	public void updatePlayers() throws IOException {
		for (Player player : gameEngine.getAllPlayers()) {
			player.getConnection().sendMessage(gameEngine.marshall());
		}
	}

	public void setGameEngine(GameEngine gameEngine) {
		this.gameEngine = gameEngine;
	}
}
