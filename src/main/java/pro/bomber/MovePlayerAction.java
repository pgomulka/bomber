package pro.bomber;

public class MovePlayerAction implements Action {

	private String id;
	private String xv;
	private String yv;

	public MovePlayerAction(String data) {
		String[] split = data.split(",");
		this.id = split[0];
		this.xv = split[1];
		this.yv = split[2];
	}

	@Override
	public void executeOn(GameEngine gameEngine) {
		gameEngine.movePlayer(id, xv, yv);
	}
}
