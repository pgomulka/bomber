/* Copyright 2010 Sabre Holdings */
package pro.bomber;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Board
{
    Set<Player> players = new HashSet<Player>();

    public void add(Player player)
    {
        players.add(player);
    }
    public void remove(Player player){
    	players.remove(player);
    }

    public boolean contains(Player player)
    {
        return players.contains(player);
    }

	public Collection<Player> getPlayers() {
		return players;
	}

	public Player getPlayer(int id) {
		for(Player player : players)
			if(player.getId()==id)
				return player;
		return null;
		
	}
}
