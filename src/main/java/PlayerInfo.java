public class PlayerInfo {
	public int id;
	public int x;
	public int y;
    private boolean dead = false;
	public PlayerInfo(int id, int x, int y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}

    public PlayerInfo(PlayerInfo info)
    {
        this.x = info.x;
        this.y = info.y;
        this.id = info.id;
    }

    public int getId() {
		return id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {

		return id + "," + x + "," + y;
	}

    public PlayerInfo move(int x, int y)
    {
        this.x+=x;
        this.y+=y;
        return this;
    }

    public void die(){
        dead = true;
    }

    public boolean isDead(){
        return dead;
    }
}
